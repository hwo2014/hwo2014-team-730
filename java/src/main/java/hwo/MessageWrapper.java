package hwo;

import com.google.gson.annotations.Expose;

/**
 * Created by austinh on 4/15/14.
 */
public class MessageWrapper {
    @Expose
    public String msgType;
    @Expose
    public Object data;

    MessageWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MessageWrapper(final Message message) {
        this(message.getType().toString(), message.getData());
    }
}

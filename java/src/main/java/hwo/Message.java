package hwo;

import com.google.gson.JsonObject;

/**
 * Created by austinh on 4/15/14.
 */
public interface Message<PayloadType> {
    String getType();
    Object getData();
    PayloadType getPayload();

    public static class Types {
        public static final String JOIN = "join";
        public static final String GAME_INIT = "gameInit";
        public static final String GAME_END = "gameEnd";
        public static final String GAME_START = "gameStart";
        public static final String CAR_POSITIONS = "carPositions";
        public static final String THROTTLE = "throttle";
        public static final String PING = "ping";
        public static final String YOUR_CAR = "yourCar";
        public static final String CRASH = "crash";
        public static final String SPAWN = "spawn";
        public static final String LAP_FINISHED = "lapFinished";
        public static final String DID_NOT_FINISH = "dnf";
        public static final String FINISH = "finish";
    }
}

package hwo.payload;

import com.google.gson.annotations.SerializedName;

/**
 * Created by austinh on 4/15/14.
 */
public class CarPosition {
    @SerializedName("id")
    private Id mId;
    @SerializedName("angle")
    private double mAngle;
    @SerializedName("piecePosition")
    private PiecePosition mPiecePosition;

    public Id getId() {
        return mId;
    }

    public double getAngle() {
        return mAngle;
    }

    public PiecePosition getPiecePosition() {
        return mPiecePosition;
    }

    public static class Id {
        @SerializedName("name")
        private String mName;
        @SerializedName("color")
        private String mColor;

        public String getName() {
            return mName;
        }

        public String getColor() {
            return mColor;
        }
    }

    public static class PiecePosition {
        @SerializedName("pieceIndex")
        private int mPieceIndex;
        @SerializedName("inPieceDistance")
        private double mInPieceDistance;
        @SerializedName("lane")
        private Lane mLane;
        @SerializedName("lap")
        private int mLap;

        public int getPieceIndex() {
            return mPieceIndex;
        }

        public double getInPieceDistance() {
            return mInPieceDistance;
        }

        public Lane getLane() {
            return mLane;
        }

        public int getLap() {
            return mLap;
        }
    }

    public static class Lane {
        @SerializedName("startLaneIndex")
        private int mStartLaneIndex;
        @SerializedName("endLaneIndex")
        private int mEndLaneIndex;

        public int getStartLaneIndex() {
            return mStartLaneIndex;
        }

        public int getEndLaneIndex() {
            return mEndLaneIndex;
        }
    }
}

package hwo.payload;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by austinh on 4/17/14.
 */
public class CarPositionPayload {
    private List<CarPosition> mCarPositions;
    private Map<String, CarPosition> mNameMapping = new HashMap<String, CarPosition>();

    public List<CarPosition> getCarPositions() {
        return mCarPositions;
    }

    public CarPosition get(String name) {
        return mNameMapping.get(name);
    }

    public static class CarPositionPayloadDeserializer implements JsonDeserializer<CarPositionPayload> {
        @Override
        public CarPositionPayload deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            final CarPositionPayload payload = new CarPositionPayload();
            final Gson gson = new Gson();

            // Deserialize the car positions
            payload.mCarPositions = gson.fromJson(jsonElement, new TypeToken<Collection<CarPosition>>() { }.getType());

            // Create the name mapping
            for (CarPosition position : payload.mCarPositions) {
                payload.mNameMapping.put(position.getId().getName(), position);
            }

            return payload;
        }
    }
}

package hwo.payload;

import com.google.gson.*;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

/**
 * Created by austinh on 4/16/14.
 */
public class GameInitPayload {
    @SerializedName("race")
    private Race mRace;

    public Race getRace() {
        return mRace;
    }
}

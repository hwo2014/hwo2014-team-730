package hwo.payload;

import com.google.gson.annotations.SerializedName;

public class Lane {
    @SerializedName("distanceFromCenter")
    private Double mDistanceFromCenter;
    @SerializedName("index")
    private Integer mIndex;

    public Double getDistanceFromCenter() {
        return mDistanceFromCenter;
    }

    public Integer getIndex() {
        return mIndex;
    }
}
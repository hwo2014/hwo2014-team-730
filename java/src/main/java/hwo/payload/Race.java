package hwo.payload;

import com.google.gson.*;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

/**
 * Created by austinh on 4/16/14.
 */
public class Race {
    private Track mTrack;
    private List<Car> mCars;
    private Session mSession;

    public Track getTrack() {
        return mTrack;
    }

    public List<Car> getCars() {
        return mCars;
    }

    public Session getSession() {
        return mSession;
    }

    private static class Session {
        @SerializedName("laps")
        private int mLaps;
        @SerializedName("maxLapTimeMs")
        private long mMaxLapTime;
        @SerializedName("quickRace")
        private boolean mQuickRace;

        public int getLaps() {
            return mLaps;
        }

        public long getMaxLapTime() {
            return mMaxLapTime;
        }

        public boolean isQuickRace() {
            return mQuickRace;
        }
    }

    public static class RaceDeserializer implements JsonDeserializer<Race> {
        @Override
        public Race deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            final Race race = new Race();
            final JsonObject raceJson = jsonElement.getAsJsonObject();
            race.mTrack = jsonDeserializationContext.deserialize(raceJson.getAsJsonObject("track"), Track.class);
            race.mCars = jsonDeserializationContext.deserialize(raceJson.getAsJsonArray("cars"), new TypeToken<Collection<Car>>(){}.getType());
            race.mSession = jsonDeserializationContext.deserialize(raceJson.getAsJsonObject("raceSession"), Session.class);
            return race;
        }
    }
}

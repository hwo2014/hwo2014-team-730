package hwo.payload;

import com.google.gson.*;

import java.lang.reflect.Type;

/**
 * Created by austinh on 4/16/14.
 */
public class Point {
    private Double mX;
    private Double mY;
    private Double mAngle;

    public Double getX() {
        return mX;
    }

    public Double getY() {
        return mY;
    }

    public Double getAngle() {
        return mAngle;
    }

    public static class PointDeserializer implements JsonDeserializer<Point> {
        @Override
        public Point deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            final Point point = new Point();
            final JsonObject pointJson = jsonElement.getAsJsonObject();
            final JsonObject positionJson = pointJson.getAsJsonObject("position");
            point.mX = positionJson.get("x").getAsDouble();
            point.mY = positionJson.get("y").getAsDouble();
            point.mAngle = pointJson.get("angle").getAsDouble();
            return point;
        }
    }
}

package hwo.payload;

import com.google.gson.annotations.SerializedName;

/**
 * Created by austinh on 4/16/14.
 */
public class Car {
    @SerializedName("id")
    private CarPosition.Id mId;
    @SerializedName("dimensions")
    private Dimensions mDimensions;

    public CarPosition.Id getId() {
        return mId;
    }

    private static class Dimensions {
        @SerializedName("length")
        private double mLength;
        @SerializedName("width")
        private double mWidth;
        @SerializedName("guideFlagPosition")
        private double mGuideFlagPosition;

        public double getLength() {
            return mLength;
        }

        public double getWidth() {
            return mWidth;
        }

        public double getGuideFlagPosition() {
            return mGuideFlagPosition;
        }
    }
}

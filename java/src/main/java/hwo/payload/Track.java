package hwo.payload;

import com.google.gson.*;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

/**
 * Created by austinh on 4/16/14.
 */
public class Track {
    private String mId;
    private String mName;
    private List<Piece> mPieces;
    private List<Lane> mLanes;
    private Point mStartingPoint;

    /* package */ Track(String id, String name, List<Piece> pieces, List<Lane> lanes, Point startingPoint) {
        mId = id;
        mName = name;
        mPieces = pieces;
        mLanes = lanes;
        mStartingPoint = startingPoint;
    }

    public String getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public List<Piece> getPieces() {
        return mPieces;
    }

    public List<Lane> getLanes() {
        return mLanes;
    }

    public Point getStartingPoint() {
        return mStartingPoint;
    }

    public static class TrackDeserializer implements JsonDeserializer<Track> {
        @Override
        public Track deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            final JsonObject trackObject = jsonElement.getAsJsonObject();
            final String id = trackObject.get("id").getAsString();
            final String name = trackObject.get("name").getAsString();
            final List<Piece> pieces = jsonDeserializationContext.deserialize(
                    trackObject.get("pieces"),
                    new TypeToken<Collection<Piece>>() { }.getType());
            final List<Lane> lanes = jsonDeserializationContext.deserialize(
                    trackObject.get("lanes"),
                    new TypeToken<Collection<Lane>>() { }.getType());
            final Point startingPoint = jsonDeserializationContext.deserialize(trackObject.get("startingPoint"), Point.class);
            return new Track(id, name, pieces, lanes, startingPoint);
        }
    }
}

package hwo.payload;

import com.google.gson.annotations.SerializedName;

public class Piece {
    @SerializedName("length")
    private Double mLength;
    @SerializedName("switch")
    private Boolean mSwitch;
    @SerializedName("radius")
    private Double mRadius;
    @SerializedName("angle")
    private Double mAngle;

    public Double getLength() {
        return mLength;
    }

    public Boolean isSwitch() {
        return mSwitch != null && mSwitch;
    }

    public Double getRadius() {
        return mRadius;
    }

    public Double getAngle() {
        return mAngle;
    }
}
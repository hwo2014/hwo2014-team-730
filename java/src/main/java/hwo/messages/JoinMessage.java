package hwo.messages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import hwo.MessageWrapper;

/**
 * Created by austinh on 4/15/14.
 */
public class JoinMessage extends AbstractMessage {
    @Expose
    @SerializedName("name")
    private String mName;
    @Expose
    @SerializedName("key")
    private String mKey;

    /* package */ JoinMessage(MessageWrapper wrapper) {
    }

    public JoinMessage(String name, String key) {
        super(Types.JOIN);
        mName = name;
        mKey = key;
    }
}

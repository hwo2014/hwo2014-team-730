package hwo.messages;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import hwo.MessageWrapper;
import hwo.payload.CarPosition;
import hwo.payload.CarPositionPayload;

import java.util.Collection;
import java.util.List;

/**
 * Created by austinh on 4/15/14.
 */
public class CarPositionMessage extends AbstractMessage<CarPositionPayload> {
    private CarPositionPayload mCarPositionPayload;

    /* package */ CarPositionMessage(MessageWrapper wrapper) {
        super(wrapper.msgType);
        final Gson gson = new GsonBuilder()
                .registerTypeAdapter(CarPositionPayload.class, new CarPositionPayload.CarPositionPayloadDeserializer())
                .create();
        mCarPositionPayload = gson.fromJson(gson.toJson(wrapper.data), CarPositionPayload.class);
    }

    @Override
    public CarPositionPayload getPayload() {
        return mCarPositionPayload;
    }

    @Override
    public String toString() {
        return new Gson().toJson(mCarPositionPayload);
    }
}

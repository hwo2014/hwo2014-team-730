package hwo.messages;

import hwo.Message;
import hwo.MessageWrapper;

/**
 * Created by austinh on 4/15/14.
 */
public class ServerMessageFactory {
    public static Message getMessage(MessageWrapper wrapper) {
        if (Message.Types.CAR_POSITIONS.equals(wrapper.msgType)) {
            return new CarPositionMessage(wrapper);
        } else if (Message.Types.JOIN.equals(wrapper.msgType)) {
            return new JoinMessage(wrapper);
        } else if (Message.Types.YOUR_CAR.equals(wrapper.msgType)) {
            return new YourCarMessage(wrapper);
        } else if (Message.Types.GAME_INIT.equals(wrapper.msgType)) {
            return new GameInitMessage(wrapper);
        }

        return new AbstractMessage(wrapper.msgType);
    }
}

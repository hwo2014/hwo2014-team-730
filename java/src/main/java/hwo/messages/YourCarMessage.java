package hwo.messages;

import com.google.gson.Gson;
import hwo.MessageWrapper;
import hwo.payload.CarPosition;
import teamname.Car;

/**
 * Created by austinh on 4/15/14.
 */
public class YourCarMessage extends AbstractMessage<CarPosition.Id> {
    private CarPosition.Id mCarId;

    /* package */ YourCarMessage(MessageWrapper wrapper) {
        super(Types.YOUR_CAR);
        final Gson gson = new Gson();
        mCarId = gson.fromJson(gson.toJson(wrapper.data), CarPosition.Id.class);
    }

    @Override
    public CarPosition.Id getPayload() {
        return mCarId;
    }
}

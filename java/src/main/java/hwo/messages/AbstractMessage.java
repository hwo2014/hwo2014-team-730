package hwo.messages;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import hwo.Message;

/**
 * Created by austinh on 4/15/14.
 */
public class AbstractMessage<PayloadType> implements Message<PayloadType> {
    private String mMessageType;

    /* package */ AbstractMessage() { }

    public AbstractMessage(String messageType) {
        mMessageType = messageType;
    }

    @Override
    public String getType() {
        return mMessageType;
    }

    @Override
    public Object getData() {
        return this;
    }

    @Override
    public PayloadType getPayload() {
        return null;
    }

    @Override
    public String toString() {
        return new Gson().toJson(getData());
    }
}

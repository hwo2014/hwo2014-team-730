package hwo.messages;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import hwo.MessageWrapper;
import hwo.payload.GameInitPayload;
import hwo.payload.Point;
import hwo.payload.Race;
import hwo.payload.Track;

import java.util.Collection;

/**
 * Created by austinh on 4/16/14.
 */
public class GameInitMessage extends AbstractMessage<GameInitPayload> {
    private GameInitPayload mGameInitPayload;

    /* package */ GameInitMessage(MessageWrapper wrapper) {
        super(wrapper.msgType);
        final Gson gson = new GsonBuilder()
                .registerTypeAdapter(Point.class, new Point.PointDeserializer())
                .registerTypeAdapter(Track.class, new Track.TrackDeserializer())
                .registerTypeAdapter(Race.class, new Race.RaceDeserializer())
                .create();
        mGameInitPayload = gson.fromJson(gson.toJson(wrapper.data), GameInitPayload.class);
    }

    @Override
    public GameInitPayload getPayload() {
        return mGameInitPayload;
    }
}

package hwo.messages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import hwo.Message;

/**
 * Created by austinh on 4/15/14.
 */
public class ThrottleMessage extends AbstractMessage {
    private double mThrottle;

    public ThrottleMessage(double throttle) {
        super(Types.THROTTLE);
        mThrottle = throttle;
    }

    @Override
    public Object getData() {
        return mThrottle;
    }
}

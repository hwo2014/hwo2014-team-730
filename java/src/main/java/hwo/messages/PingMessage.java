package hwo.messages;

/**
 * Created by austinh on 4/15/14.
 */
public class PingMessage extends AbstractMessage {
    public PingMessage() {
        super(Types.PING);
    }
}

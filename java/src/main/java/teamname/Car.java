package teamname;

import hwo.Message;

/**
 * Created by austinh on 4/15/14.
 */
public interface Car {
    String getName();

    String getColor();

    double getLength();

    double getWidth();

    double getMass();

    double getAngle();

    double getPivotRadius();

    double getDistance();

    double getVelocity();

    double getAverageVelocity();

    double getAcceleration();

    double getAverageAcceleration();

    double getTireFriction();

    int getThrottle();

    int getLane();

    void setTrack(Track track);

    void handleMessage(Message message, float time, float delta);
}

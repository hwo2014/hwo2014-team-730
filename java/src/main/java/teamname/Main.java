package teamname;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import hwo.Message;
import hwo.MessageWrapper;
import hwo.messages.JoinMessage;
import hwo.messages.ServerMessageFactory;
import teamname.simulators.SimpleSimulator;

import java.io.*;
import java.net.Socket;

/**
 * Created by austinh on 4/15/14.
 */
public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new JoinMessage(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    private float lastTimestamp = 0;

    public Main(final BufferedReader reader, final PrintWriter writer, final JoinMessage join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        final Simulator simulator = new SimpleSimulator();

        while((line = reader.readLine()) != null) {
            final MessageWrapper msgFromServer = gson.fromJson(line, MessageWrapper.class);
            final Message message = ServerMessageFactory.getMessage(msgFromServer);

            final float timestamp = lastTimestamp + 1/60f;
            final Message response = simulator.handleMessage(
                    message,
                    timestamp,
                    lastTimestamp != -1 ? timestamp - lastTimestamp : 0);
            lastTimestamp = timestamp;
            if (response != null) {
                send(response);
            }
        }
    }

    private void send(final Message msg) {
        final Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        final String json = gson.toJson(new MessageWrapper(msg));
        System.out.println(json);
        writer.println(json);
        writer.flush();
    }
}

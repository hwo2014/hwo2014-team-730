package teamname;

import hwo.payload.Lane;

import java.util.*;

/**
 * Created by austinh on 4/15/14.
 */
public class Track implements List<Segment> {
    private List<Segment> mSegments = new ArrayList<Segment>();
    private List<Lane> mLanes = new ArrayList<Lane>();

    public Track(List<Lane> lanes) {
        mLanes = lanes;
    }

    public List<Lane> getLanes() {
        return mLanes;
    }

    @Override
    public int size() {
        return mSegments.size();
    }

    @Override
    public boolean isEmpty() {
        return mSegments.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return mSegments.contains(o);
    }

    @Override
    public Iterator<Segment> iterator() {
        return mSegments.iterator();
    }

    @Override
    public Object[] toArray() {
        return mSegments.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return mSegments.toArray(a);
    }

    @Override
    public boolean add(Segment segment) {
        return mSegments.add(segment);
    }

    @Override
    public boolean remove(Object o) {
        if (!(o instanceof Segment)) {
            return false;
        }

        final Segment segment = (Segment) o;

        return mSegments.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return mSegments.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends Segment> c) {
        return mSegments.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends Segment> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        // TODO : austinh : update length
        return mSegments.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return mSegments.retainAll(c);
    }

    @Override
    public void clear() {
        mSegments.clear();
    }

    @Override
    public Segment get(int index) {
        return mSegments.get(index);
    }

    @Override
    public Segment set(int index, Segment element) {
        // TODO : austinh : update length
        return mSegments.set(index, element);
    }

    @Override
    public void add(int index, Segment element) {
        // TODO : austinh : update length
        mSegments.add(index, element);
    }

    @Override
    public Segment remove(int index) {
        // TODO : austinh : update length
        return mSegments.remove(index);
    }

    @Override
    public int indexOf(Object o) {
        return mSegments.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return mSegments.lastIndexOf(o);
    }

    @Override
    public ListIterator<Segment> listIterator() {
        return mSegments.listIterator();
    }

    @Override
    public ListIterator<Segment> listIterator(int index) {
        return mSegments.listIterator(index);
    }

    @Override
    public List<Segment> subList(int fromIndex, int toIndex) {
        return mSegments.subList(fromIndex, toIndex);
    }
}

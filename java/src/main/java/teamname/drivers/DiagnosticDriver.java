package teamname.drivers;

import hwo.Message;
import teamname.Car;
import teamname.Segment;
import teamname.Track;
import teamname.cars.Fleet;

/**
 * Created by austinh on 4/17/14.
 */
public class DiagnosticDriver extends AbstractDriver {
    private double mThrottle = 0;

    @Override
    public void onInit(Car car, Fleet allCars, Track track, float time) {
        super.onInit(car, allCars, track, time);
    }

    @Override
    public void onStart(float time, float delta) {

    }

    @Override
    public Message onTick(float time, float delta) {
        return null;
    }

    @Override
    public void onLap(int lap, float time, float delta) {

    }

    @Override
    public void onFinish(float time, float delta) {

    }


}

package teamname.drivers;

import hwo.Message;
import teamname.Car;
import teamname.Driver;
import teamname.Track;
import teamname.cars.Fleet;

import java.util.List;

/**
 * Created by austinh on 4/17/14.
 */
public abstract class AbstractDriver implements Driver {
    private Fleet mAllCars;
    private Car mCar;
    private Track mTrack;

    public Fleet getAllCars() {
        return mAllCars;
    }

    public Car getCar() {
        return mCar;
    }

    public Track getTrack() {
        return mTrack;
    }

    @Override
    public void onInit(Car car, Fleet allCars, Track track, float time) {
        mCar = car;
        mAllCars = allCars;
        mTrack = track;
    }
}

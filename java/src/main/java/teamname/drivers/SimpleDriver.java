package teamname.drivers;

import hwo.Message;
import hwo.messages.ThrottleMessage;

/**
 * Created by austinh on 4/17/14.
 */
public class SimpleDriver extends AbstractDriver {

    @Override
    public void onStart(float time, float delta) {

    }

    @Override
    public Message onTick(float time, float delta) {
        return new ThrottleMessage(0.55f);
    }

    @Override
    public void onFinish(float time, float delta) {

    }

    @Override
    public void onLap(int lap, float time, float delta) {

    }
}

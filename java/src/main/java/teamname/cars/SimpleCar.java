package teamname.cars;

import hwo.Message;
import hwo.messages.CarPositionMessage;
import hwo.messages.PingMessage;
import hwo.messages.ThrottleMessage;
import hwo.payload.CarPosition;
import teamname.Track;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by austinh on 4/15/14.
 */
public class SimpleCar extends AbstractCar {
    private PrintWriter pw;

    public SimpleCar(String name, String color) {
        super(name, color);
        try {
            pw = new PrintWriter(new BufferedWriter(new FileWriter("/tmp/data.csv", false)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void handleOtherMessage(Message message, float time, float delta) {
        if (Message.Types.GAME_END.equals(message.getType())) {
            pw.close();
        }

        super.handleOtherMessage(message, time, delta);
    }

    @Override
    protected void onUpdate(CarPosition position, List<CarPosition> positions, float time, float delta) {
        System.out.println(String.format("d: %f, v: %f, a: %f, w: %f, t: %f", getDistance(), getAverageVelocity(), getAverageAcceleration(), getAngle(), time));
        pw.println(String.format("%f, %f, %f, %f, %f", time, getDistance(), getAverageVelocity(), getAverageAcceleration(), getAngle()));
    }
}

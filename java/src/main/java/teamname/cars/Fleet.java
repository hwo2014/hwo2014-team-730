package teamname.cars;

import teamname.Car;

import java.util.*;

/**
 * Created by austinh on 4/17/14.
 */
public class Fleet implements List<Car> {
    private final List<Car> mCarList;
    private final Map<String, Car> mNameMapping;

    public Fleet() {
        mCarList = new ArrayList<Car>();
        mNameMapping = new HashMap<String, Car>();
    }

    public Fleet(List<Car> cars) {
        mCarList = new ArrayList<Car>(cars);
        mNameMapping = new HashMap<String, Car>();

        // Initialize the name mapping
        for (Car car : cars) {
            mNameMapping.put(car.getName(), car);
        }
    }

    @Override
    public int size() {
        return mCarList.size();
    }

    @Override
    public boolean isEmpty() {
        return mCarList.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        if (!(o instanceof Car)) {
            return false;
        }

        final Car car = (Car) o;
        return mNameMapping.containsKey(car.getName());
    }

    @Override
    public Iterator<Car> iterator() {
        return mCarList.iterator();
    }

    @Override
    public Object[] toArray() {
        return mCarList.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return mCarList.toArray(a);
    }

    @Override
    public boolean add(Car car) {
        mNameMapping.put(car.getName(), car);
        return mCarList.add(car);
    }

    @Override
    public boolean remove(Object o) {
        if (!(o instanceof Car)) {
            return false;
        }

        final Car car = (Car) o;
        mNameMapping.remove(car.getName());
        return mCarList.remove(car);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return mCarList.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends Car> c) {
        for (Car car : c) {
            mNameMapping.put(car.getName(), car);
        }
        return mCarList.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends Car> c) {
        for (Car car : c) {
            mNameMapping.put(car.getName(), car);
        }
        return mCarList.addAll(index, c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object o : c) {
            if (!(o instanceof Car)) {
                continue;
            }
            final Car car = (Car) o;
            mNameMapping.remove(car.getName());
        }
        return mCarList.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        mNameMapping.clear();
        mCarList.clear();
    }

    @Override
    public Car get(int index) {
        return mCarList.get(index);
    }

    @Override
    public Car set(int index, Car element) {
        return mCarList.set(index, element);
    }

    @Override
    public void add(int index, Car element) {
        mNameMapping.put(element.getName(), element);
        mCarList.add(element);
    }

    @Override
    public Car remove(int index) {
        final Car car = mCarList.remove(index);
        if (car != null) {
            mNameMapping.remove(car.getName());
        }
        return car;
    }

    @Override
    public int indexOf(Object o) {
        return mCarList.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return mCarList.lastIndexOf(o);
    }

    @Override
    public ListIterator<Car> listIterator() {
        return mCarList.listIterator();
    }

    @Override
    public ListIterator<Car> listIterator(int index) {
        return mCarList.listIterator(index);
    }

    @Override
    public List<Car> subList(int fromIndex, int toIndex) {
        return mCarList.subList(fromIndex, toIndex);
    }
}

package teamname.cars;

import hwo.Message;
import hwo.payload.CarPosition;
import teamname.Track;

import java.util.List;

/**
 * Created by austinh on 4/15/14.
 */
public class DiagnosticCar extends AbstractCar {
    public DiagnosticCar(String name, String color, Track track) {
        super(name, color, track);
    }

    @Override
    protected Message onUpdate(CarPosition position, List<CarPosition> positions, long time, long delta) {
        return null;
    }
}

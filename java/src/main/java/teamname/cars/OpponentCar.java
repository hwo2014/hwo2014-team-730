package teamname.cars;

import hwo.Message;
import hwo.payload.CarPosition;

import java.util.List;

/**
 * Created by austinh on 4/16/14.
 */
public class OpponentCar extends AbstractCar {
    public OpponentCar(hwo.payload.Car car) {
        super(car.getId().getName(), car.getId().getColor());
    }

    @Override
    protected void onUpdate(CarPosition position, List<CarPosition> positions, float time, float delta) {
        // Nothing
    }
}

package teamname.cars;

import com.sun.tools.javac.util.ListBuffer;
import hwo.Message;
import hwo.messages.CarPositionMessage;
import hwo.messages.PingMessage;
import hwo.payload.CarPosition;
import teamname.Car;
import teamname.Track;
import teamname.util.CappedQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * Created by austinh on 4/15/14.
 */
public abstract class AbstractCar implements Car {
    private static final int HYSTERESIS_SIZE = 5;

    /**
     * |\-----------------------------\
     * |  \              \              \
     * |    \           Width             \
     * |      \ ----------\-----------------\
     * |\      |                            |
     *    \    |<--------  Length  -------->|
     *      \  |    OOO              OOO    |
     *        \|---OOOOO------------OOOOO---|
     *              OOO              OOO
     */

    private final String mName;
    private final String mColor;

    private int mLaneIndex;

    private double mLength;
    private double mWidth;
    private double mMass;

    private double mAngle;

    private double mPivotRadius;

    private int mLastPieceIndex = -1;
    private int mPieceIndex = -1;

    private CappedQueue<Double> mDistanceHistory = new CappedQueue<Double>(HYSTERESIS_SIZE);
    private double mLastInPieceDistance;
    private double mLastDistance;
    private double mDistance;

    private CappedQueue<Double> mVelocityHistory = new CappedQueue<Double>(HYSTERESIS_SIZE);
    private double mLastVelocity;
    private double mAverageVelocity;
    private double mVelocity;

    private double mLastAcceleration;
    private double mAverageAcceleration;
    private double mAcceleration;

    private double mTireFriction;

    private int mThrottle;

    private int mLane;

    private Track mTrack;

    public AbstractCar(String name, String color) {
        mName = name;
        mColor = color;
    }

    public void setTrack(Track track) {
        mTrack = track;
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public String getColor() {
        return mColor;
    }

    @Override
    public double getLength() {
        return mLength;
    }

    @Override
    public double getWidth() {
        return mWidth;
    }

    @Override
    public double getMass() {
        return mMass;
    }

    @Override
    public double getAngle() {
        return mAngle;
    }

    @Override
    public double getPivotRadius() {
        return mPivotRadius;
    }

    @Override
    public double getDistance() {
        return mDistance;
    }

    @Override
    public double getVelocity() {
        return mVelocity;
    }

    @Override
    public double getAverageVelocity() {
        return mAverageVelocity;
    }

    @Override
    public double getAcceleration() {
        return mAcceleration;
    }

    @Override
    public double getAverageAcceleration() {
        return mAverageAcceleration;
    }

    @Override
    public double getTireFriction() {
        return mTireFriction;
    }

    @Override
    public int getThrottle() {
        return mThrottle;
    }

    @Override
    public int getLane() {
        return mLane;
    }

    protected Track getTrack() {
        return mTrack;
    }

    protected abstract void onUpdate(CarPosition position, List<CarPosition> positions, float time, float delta);

    private void handleMessage(CarPositionMessage message, float time, float delta) {
        // Locate the current car's position
        CarPosition carPosition = message.getPayload().get(mName);

        // Calculate the current stats for our updated position
        crunchPosition(carPosition, delta);

        // Call the onUpdate method
        onUpdate(carPosition, message.getPayload().getCarPositions(), time, delta);
    }

    private void crunchPosition(CarPosition position, float delta) {
        mLaneIndex = position.getPiecePosition().getLane().getEndLaneIndex();
        mAngle = position.getAngle() * Math.PI / 180D;

        mLastDistance = mDistance;
        mDistanceHistory.add(mDistance);

        mLastPieceIndex = mPieceIndex;
        mPieceIndex = position.getPiecePosition().getPieceIndex();

        // Check for movement to the next piece
        if (mPieceIndex != mLastPieceIndex) {
            System.out.println(String.format("Next segment: %s [%d] (%s)", getTrack().get(mPieceIndex).getType(), mPieceIndex, getTrack().get(mPieceIndex).isSwitch()));
            if (mLastPieceIndex > -1) {
                // We need to add the rest of the last piece to our travelled distance
                mDistance += getTrack().get(mLastPieceIndex).getTrackLength(mLaneIndex) - mLastInPieceDistance;
            }

            // Set our last in piece distance
            mLastInPieceDistance = position.getPiecePosition().getInPieceDistance();

            // Add our current in piece distance
            mDistance += mLastInPieceDistance;
        } else {
            // We're on the same segment, add the difference between our current in piece distance
            // and our old in piece distance
            mDistance += position.getPiecePosition().getInPieceDistance() - mLastInPieceDistance;

            // Set our last in piece distance
            mLastInPieceDistance = position.getPiecePosition().getInPieceDistance();
        }

        mLastVelocity = mVelocity;
        mVelocityHistory.add(mVelocity);

        mVelocity = (mDistance - mLastDistance) / delta;
        mAverageVelocity = (mDistance - mDistanceHistory.peek()) / delta;

        mLastAcceleration = mAcceleration;
        mAcceleration = (mVelocity - mLastVelocity) / delta;
        mAverageAcceleration = (mVelocity - mVelocityHistory.peek()) / delta;
    }

    protected void handleOtherMessage(Message message, float time, float delta) {
        // Nothing
    }

    @Override
    public void handleMessage(Message message, float time, float delta) {
        final String messageType = message.getType();
        if (hwo.Message.Types.CAR_POSITIONS.equals(messageType)) {
            handleMessage((CarPositionMessage) message, time, delta);
        } else {
            handleOtherMessage(message, time, delta);
        }
    }
}

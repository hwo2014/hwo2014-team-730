package teamname;

import hwo.Message;
import teamname.cars.Fleet;

import java.util.List;

/**
 * Created by austinh on 4/17/14.
 */
public interface Driver {
    void onInit(Car car, Fleet allCars, Track track, float time);
    void onStart(float time, float delta);
    Message onTick(float time, float delta);
    void onLap(int lap, float time, float delta);
    void onFinish(float time, float delta);

    void setThrottle(double throttle);
    void switchLane(SwitchDirection direction);

    public enum SwitchDirection {
        Left,
        Right
    }
}

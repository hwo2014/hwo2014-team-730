package teamname;

import hwo.Message;

/**
 * Created by austinh on 4/15/14.
 */
public interface Simulator {
    Message handleMessage(Message message, float time, float delta);
}

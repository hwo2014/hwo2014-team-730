package teamname;

import hwo.payload.Lane;
import hwo.payload.Piece;

import java.util.List;

/**
 * Created by austinh on 4/15/14.
 */
public class Segment {
    private SegmentType mType;
    private boolean mIsSwitch;
    private double mTrackLength;
    private double mRadius;
    private double mAngle;
    private List<Lane> mLanes;

    private Segment(SegmentType type) {
        mType = type;
    }

    public static Segment from(Piece piece, List<Lane> lanes) {
        final Segment segment;
        if (piece.getRadius() != null) {
            segment = new Segment(SegmentType.Turn);
            segment.mRadius = piece.getRadius();
            segment.mAngle = piece.getAngle() * Math.PI / 180d;
        } else {
            segment = new Segment(SegmentType.Straight);
            segment.mTrackLength = piece.getLength();
        }
        segment.mLanes = lanes;
        segment.mIsSwitch = piece.isSwitch();

        return segment;
    }

    public SegmentType getType() {
        return mType;
    }

    public void setType(SegmentType type) {
        mType = type;
    }

    public boolean isSwitch() {
        return mIsSwitch;
    }

    public void setIsSwitch(boolean aSwitch) {
        mIsSwitch = aSwitch;
    }

    public double getTrackLength(int laneIndex) {
        if (mType == SegmentType.Straight) {
            return mTrackLength;
        } else {
            return Math.abs(mAngle) * (mRadius - ((mAngle < 0 ? -1 : 1) * mLanes.get(laneIndex).getDistanceFromCenter()));
        }
    }

    public void setTrackLength(double trackLength) {
        mTrackLength = trackLength;
    }

    public double getRadius() {
        return mRadius;
    }

    public void setRadius(double radius) {
        mRadius = radius;
    }

    public double getAngle() {
        return mAngle;
    }

    public void setAngle(double angle) {
        mAngle = angle;
    }

    public boolean laneToLeft(int laneIndex) {
        return laneIndex > 0;
    }

    public boolean laneToRight(int laneIndex) {
        return laneIndex < mLanes.size();
    }

    public enum SegmentType {
        Straight,
        Turn
    }
}

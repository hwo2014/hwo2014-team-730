package teamname.util;

import java.util.*;

/**
 * Created by austinh on 4/16/14.
 */
public class CappedQueue<Type> implements Queue<Type> {
    private final Deque<Type> mQueue = new LinkedList<Type>();
    private final int mCapacity;

    public CappedQueue(int capacity) {
        mCapacity = capacity;
    }

    @Override
    public boolean add(Type type) {
        final boolean result = mQueue.add(type);
        while (mQueue.size() > mCapacity) {
            mQueue.removeFirst();
        }

        return result;
    }

    @Override
    public boolean offer(Type type) {
        return add(type);
    }

    @Override
    public Type remove() {
        if (mQueue.isEmpty()) {
            throw new NoSuchElementException();
        } else {
            return mQueue.removeLast();
        }
    }

    @Override
    public Type poll() {
        if (mQueue.isEmpty()) {
            return null;
        } else {
            return mQueue.removeLast();
        }
    }

    @Override
    public Type element() {
        if (mQueue.isEmpty()) {
            throw new NoSuchElementException();
        } else {
            return mQueue.getLast();
        }
    }

    @Override
    public Type peek() {
        if (mQueue.isEmpty()) {
            throw new NoSuchElementException();
        } else {
            return mQueue.getLast();
        }
    }

    @Override
    public int size() {
        return mQueue.size();
    }

    @Override
    public boolean isEmpty() {
        return mQueue.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return mQueue.contains(o);
    }

    @Override
    public Iterator<Type> iterator() {
        return mQueue.iterator();
    }

    @Override
    public Object[] toArray() {
        return mQueue.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return mQueue.toArray(a);
    }

    @Override
    public boolean remove(Object o) {
        return mQueue.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return mQueue.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends Type> c) {
        return mQueue.addAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return mQueue.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return mQueue.retainAll(c);
    }

    @Override
    public void clear() {
        mQueue.clear();
    }
}

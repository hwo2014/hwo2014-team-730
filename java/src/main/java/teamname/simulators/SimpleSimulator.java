package teamname.simulators;

import hwo.Message;
import hwo.messages.CarPositionMessage;
import hwo.messages.GameInitMessage;
import hwo.messages.JoinMessage;
import hwo.messages.YourCarMessage;
import hwo.payload.Piece;
import teamname.Car;
import teamname.Driver;
import teamname.Segment;
import teamname.Track;
import teamname.cars.Fleet;
import teamname.cars.OpponentCar;
import teamname.cars.SimpleCar;
import teamname.drivers.SimpleDriver;

/**
 * Created by austinh on 4/15/14.
 */
public class SimpleSimulator extends AbstractSimulator {
    public SimpleSimulator() {
        super(new SimpleDriver());
    }

    @Override
    protected Car createCar(String name, String color) {
        return new SimpleCar(name, color);
    }
}

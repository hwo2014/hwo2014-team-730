package teamname.simulators;

import hwo.Message;
import hwo.messages.*;
import hwo.payload.CarPosition;
import hwo.payload.Piece;
import teamname.*;
import teamname.cars.Fleet;
import teamname.cars.OpponentCar;
import teamname.cars.SimpleCar;

/**
 * Created by austinh on 4/15/14.
 */
public abstract class AbstractSimulator implements Simulator {
    private Fleet mFleet;
    private Track mTrack;
    private Car mCar;
    private Driver mDriver;

    protected AbstractSimulator(Driver driver) {
        mDriver = driver;
    }

    public Message handleMessage(JoinMessage message, float time, float delta) {
        return null;
    }

    public Message handleMessage(CarPositionMessage message, float time, float delta) {
        // Update cars
        for (Car car : mFleet) {
            car.handleMessage(message, time, delta);
        }

        // Let the driver of our car decide what to do
        return mDriver.onTick(time, delta);
    }

    public Message handleMessage(YourCarMessage message, float time, float delta) {
        mCar = createCar(message.getPayload().getName(), message.getPayload().getColor());
        return null;
    }

//    public abstract Message handleMessage(GameEndMessage message);
//    public abstract Message handleMessage(GameStartMessage message);

    public Message handleMessage(GameInitMessage message, float time, float delta) {
        mTrack = new Track(message.getPayload().getRace().getTrack().getLanes());
        for (Piece piece : message.getPayload().getRace().getTrack().getPieces()) {
            mTrack.add(Segment.from(piece, message.getPayload().getRace().getTrack().getLanes()));
        }

        // Set the car's track
        mCar.setTrack(mTrack);

        // Create the fleet of cars
        mFleet = new Fleet();

        // Add our car
        mFleet.add(mCar);

        // Add all of the opponent cars
        for (hwo.payload.Car car : message.getPayload().getRace().getCars()) {
            // Ignore our car
            if (car.getId().getName().equals(mCar)) {
                continue;
            }

            final Car opponent = new OpponentCar(car);
            opponent.setTrack(mTrack);
            mFleet.add(opponent);
        }

        // Initialize the driver
        mDriver.onInit(mCar, mFleet, mTrack, time);

        return null;
    }
//    public abstract Message handleMessage(SpawnMessage message);
//    public abstract Message handleMessage(CrashMessage message);
//    public abstract Message handleMessage(FinishMessage message);
//    public abstract Message handleMessage(LapFinishedMessage message);


    @Override
    public Message handleMessage(Message message, float time, float delta) {
        final String messageType = message.getType();

        if (Message.Types.JOIN.equals(messageType)) {
            return handleMessage((JoinMessage) message, time, delta);
        } else if (Message.Types.YOUR_CAR.equals(messageType)) {
            return handleMessage((YourCarMessage) message, time, delta);
        } else if (Message.Types.CAR_POSITIONS.equals(messageType)) {
            return handleMessage((CarPositionMessage) message, time, delta);
        } else if (Message.Types.GAME_INIT.equals(messageType)) {
            return handleMessage((GameInitMessage) message, time, delta);
        } else {
            return handleOtherMessage(message, time, delta);
        }
    }

    public Message handleOtherMessage(Message message, float time, float delta) {
        return new PingMessage();
    }

    protected abstract Car createCar(String name, String color);
}
